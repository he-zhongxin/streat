# import socket
#
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.bind(("127.0.0.1", 8001))
# s.listen()
# conn, address = s.accept()
# data = conn.recv(1024)
# print(data.decode())
# conn.sendall(bytes("已接受到信息", encoding='utf-8')+bytes(data.decode(), encoding='utf-8'))
# s.close()


import socket

s = socket.socket()

s.bind(("127.0.0.1", 8080))
s.listen(5)

while True:
    conn, address = s.accept()
    conn.sendall(bytes("已连接到服务端", encoding="utf-8"))

    size = conn.recv(1024)
    sizestr = str(size, encoding="utf-8")
    size1 = int(sizestr)
    conn.sendall(bytes("开始传输文件", encoding="utf-8"))
    size2 = 0
    f = open("20201310new.txt", "wb")
    while True:
        if size1 == size2:
            break
        date = conn.recv(1024)
        f.write(date)
        size2 += len(date)

    conn.sendall(bytes("传输完成", encoding="utf-8"))
    f.close()


# import socket
#
# sk = socket.socket()
#
# sk.bind(("127.0.0.1",8080))
# sk.listen(5)
#
# while True:
#     conn,address = sk.accept()
#     conn.sendall(bytes("欢迎光临我爱我家",encoding="utf-8"))
#
#     size = conn.recv(1024)
#     size_str = str(size,encoding="utf-8")
#     file_size = int(size_str)
#
#     conn.sendall(bytes("开始传送", encoding="utf-8"))
#
#     has_size = 0
#     f = open("20201316new.txt","wb")
#     while True:
#         if file_size == has_size:
#             break
#         date = conn.recv(1024)
#         f.write(date)
#         has_size += len(date)
#
#     f.close()